## Title: Movable Pet Bar|cff00aa00 v9.0.1.0|r
## classic ## Title: Movable Pet Bar|cff00aa00 v1.0.0.1|r
## Interface: 90001
## Version: 9.0.1.0
## clasic ## Version: 1.0.0.1
## classic ## Interface: 11305
## Author: Yunohu
## Notes: A simple movable and adjustable pet bar replacement for WoW Classic
## SavedVariablesPerCharacter: MPB_PetBar, MPB_Locked, Axis_Hor, MPB_IsOn
## X-Date: 2020-10-29
MovablePetBar.xml
