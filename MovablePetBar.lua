-----------------------------------------------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------------------------------------------

local Temped_PetAktionBarFrame_Update
MPB_PetBar = true;
MPB_Locked = true;
Axis_Hor = true;
MPB_Locked = false;
MPB_IsOn = true;

-- Changes the PetActionBarFrame parent to UIParent
function ReleasePetBar()
	if (PetActionBarFrame:GetParent() ~= UIParent) then
		PetActionBarFrame:SetParent(UIParent);
		PetActionBarFrame:Show();
	end
end

-----------------------------------------------------------------------------------------------------------------
-- On_Load
-----------------------------------------------------------------------------------------------------------------

	function MovablePetBar_OnLoad(self)
		if( DEFAULT_CHAT_FRAME ) then
			DEFAULT_CHAT_FRAME:AddMessage("|cff00bfffMovable Pet Bar|r |cff00ff00- loaded.|r |cff00bfffType|r '|cffffd700/mpb|r' |cff00bffffor help menu.|r");
		end
		MovablePetBar_INI();

		SlashCmdList["MOVABLEPETBARCOMMAND"] = CustomHideBar_SlashHandler;
		SLASH_MOVABLEPETBARCOMMAND1 = "/mpb";

		self:RegisterEvent("VARIABLES_LOADED");
	end

-----------------------------------------------------------------------------------------------------------------
-- On_Event
-----------------------------------------------------------------------------------------------------------------

	function MovablePetBar_OnEvent(self, event, ...)
		ReleasePetBar();
		if (event == "VARIABLES_LOADED") then
			if (MPB_PetBar == false) then
				MPB_HideBar("MPBPetBar");
			end
			if (MPB_Locked == true) then
				MPB_LockBars();
			end
			if (Axis_Hor == false) then
				MPB_ConstructPetBar_Vert();
			end
			if (MPB_IsOn == false) then
				MPB_Off();
			end
		end
	end



	function MovablePetBar_INI()
		Temped_PetActionBarFrame_Update = PetActionBarFrame_Update
		MPB_HidePetArt();

	end



	function MPB_HidePetArt()
		PetActionBarFrame:EnableMouse("false")

		SlidingActionBarTexture0:SetAlpha(0)
		SlidingActionBarTexture1:SetAlpha(0)
	end


	function MPB_On()
		MPB_HidePetArt();
		MPB_ConstructPetBar();
		MPB_ShowBar("MPBPetBar");
	end

	function MPB_Off()

		SlidingActionBarTexture0:SetAlpha(1)
		SlidingActionBarTexture1:SetAlpha(1)

		PetActionBarFrame:EnableMouse("true")

		for i=1, 12 do
			pet_button = getglobal("PetActionButton"..i);
			if (pet_button ~= nil) then
				pet_button:ClearAllPoints();
				x_coord = 65 + ((i - 1) * 38);
				y_coord = 0;
				pet_button:SetPoint("BottomRIGHT", PetActionBarFrame, "BottomLEFT", x_coord, y_coord);
			end
		end
		MPBPetBarDragButton:Hide();
	end
	

	function MPB_ConstructPetBar()
		for i=1, 12 do
			pet_button = getglobal("PetActionButton"..i);
			if (pet_button ~= nil) then
				pet_button:ClearAllPoints();
				x_coord = 2 + ((i - 1) * 33);
				y_coord = -1;
				pet_button:SetPoint("BOTTOMLEFT", MPBPetBar, "TOPLEFT", x_coord, y_coord);
			end
		end
	end

	function MPB_ConstructPetBar_Vert()
		for i=1, 12 do
			pet_button = getglobal("PetActionButton"..i);
			if (pet_button ~= nil) then
				pet_button:ClearAllPoints();					
				x_coord = -1;
				y_coord = -(2 + ((i - 1) * 33));
				pet_button:SetPoint("TOPRIGHT", MPBPetBar, "TOPLEFT", x_coord, y_coord);
			end
		end
	end


	function MPB_LinkFrameToDragButton()
		MPBPetBar:ClearAllPoints();
		MPBPetBar:SetPoint("TOPLEFT", MPBPetBarDragButton, "TOPLEFT");
	end


	function MPB_ChatMessage(message)
		if( DEFAULT_CHAT_FRAME ) then
			DEFAULT_CHAT_FRAME:AddMessage(message);
		end
	end
-----------------------------------------------------------------------------------------------------------------
-- Slash Commands
-----------------------------------------------------------------------------------------------------------------

	function CustomHideBar_SlashHandler(msg)
		if (msg == "hide") then
			MPB_HideBar("MPBPetBar");
			MPB_PetBar = false;
		elseif (msg == "show") then
			MPB_ShowBar("MPBPetBar");
			MPB_PetBar = true;
		elseif (msg == "lock") then
			MPB_LockBars();
			MPB_Locked = true;
		elseif (msg == "unlock") then
			MPB_UnLockBars();
			MPB_Locked = false;
		elseif (msg == "on") then
			MPB_On();
			MPB_IsOn = true;
		elseif (msg == "off") then
			MPB_Off();
			MPB_IsOn = false;
		elseif (msg == "vert") then
			MPB_ConstructPetBar_Vert();
			Axis_Hor = false;
		elseif (msg == "horz") then
			MPB_ConstructPetBar();
			Axis_Hor = true;
		else
			mpb_PrintHelp();
		end
	end


	function MPB_HideBar(MPBPetBar)
		getglobal(MPBPetBar):ClearAllPoints();
		getglobal(MPBPetBar):SetPoint("LEFT", "PetActionBarFrame", "Top", 0, -150);
		MPBPetBarDragButton:Hide();
		PetActionBarFrame:Hide();
	end

	function MPB_ShowBar(bar)
		PetActionBarFrame:Show();
		MPB_LinkFrameToDragButton(bar.."DragButton", bar, "RIGHT");
		if (MPB_Locked == true) then
			MPB_LockBars();
		else
			MPB_UnLockBars();
		end
			

	end

	function MPB_LockBars()
        
	        MPBPetBarDragButton:Hide();
		MPB_Locked = true;

	end        

	function MPB_UnLockBars()

	        MPBPetBarDragButton:Show();
		MPB_Locked = false;
        
	end
-----------------------------------------------------------------------------------------------------------------
-- Help Menu
-----------------------------------------------------------------------------------------------------------------        

	function mpb_PrintHelp()
	        MPB_ChatMessage(" /mpb |cff00ff00on|r / |cffff0000off|r  -------- enables or disables this addon.");
	        MPB_ChatMessage(" /mpb |cffffd700horz|r / |cff00BFFFvert|r ---- aligns petbar horizontally or vertically.");
	        MPB_ChatMessage(" /mpb |cff00BFFFshow|r / |cffffd700hide|r   -- shows or hides the petbar.");
	        MPB_ChatMessage(" /mpb |cffff0000lock|r / |cff00ff00unlock|r - locks or unlocks petbar");
	        MPB_ChatMessage(" ***|cffFFD700Individual frames are locked/unlocked with the interface actionbar lock.|r***")
	end